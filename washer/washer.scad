/* inner_diam = 10.4; */
inner_diam = 12.15;

outer_diam = 14.8;

height = 2.5;
/* height = 1.5; */

$fn=100;

difference()
{
    cylinder(h=height, r=outer_diam/2);
    translate([0,0,-0.5])
        cylinder(h=height*2, r=inner_diam/2);
}
