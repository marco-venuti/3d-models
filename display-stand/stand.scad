height    = 100;
base      = 60;
thickness = 2;
width     = 8;
gap_clear = 0.2;
gap_frac  = 0.5;
indent_r  = 6;
cut       = 3;

gap = thickness + gap_clear;

module stand(chirality) {
    linear_extrude(thickness)
    union() {
        translate([indent_r, width])
            difference() {
            circle(indent_r);
            square([indent_r, indent_r]);
        }
        difference() {
            square([base, height]);
            translate([-width, width])
                square([base, height]);
            translate([
                          base - (width + gap)/2,
                          height* (chirality == "L" ? 1 - gap_frac : 0)
                          ])
                square([gap, height* (chirality == "L" ? gap_frac : 1 - gap_frac)]);
            cut_angle = atan2(base-2*width, height-width);
            translate([base-width, 100, 0])
                rotate([0, 0, -cut_angle])
                square([cut*2, height/5], center=true);

        }
    }
}

translate([base + 10, 0, 0]) stand("L");
stand("R");
