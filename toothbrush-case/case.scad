radius = 15;
inner_side = 48;
height = 205;
thickness = 2;
indent = 25;
tolerance = 0.15;
chamfer_h = 3;

$fn = 50;


module inner(_inner_side, _radius) {
    circ_disp = _inner_side/2-_radius;
    hull()
        for (i=[-1,1], j=[-1,1])
            translate([i*circ_disp, j*circ_disp])
                circle(_radius);
}

module base(_inner_side, _radius, _thickness) {
    offset(r=_thickness)
        inner(_inner_side, radius);
}

module side(_inner_side, _radius, _thickness, _height) {
    linear_extrude(_height) {
        difference() {
            base(_inner_side, _radius, _thickness);
            inner(_inner_side, _radius);
        }
    }
}

module indent(_inner_side, _radius, _thickness, _indent, _chamfer_h, _tolerance) {
    linear_extrude(_indent) {
        difference() {
            inner(_inner_side-_tolerance, _radius);
            offset(r=-_thickness)
                inner(_inner_side-_tolerance, _radius);
        }
    }

    mirror([0,0,1])
        linear_extrude(_chamfer_h, scale=_inner_side/(_inner_side-_thickness)) {
        difference() {
            inner(_inner_side, _radius);
            offset(r=-_thickness)
                inner(_inner_side, _radius);
        }
    }
}

module sotto(_inner_side, _radius, _thickness, _height, _indent, _chamfer_h, _tolerance) {
    union()
    {
        linear_extrude(_thickness)
            base(_inner_side, _radius, _thickness);
        side(_inner_side, _radius, _thickness, _height/2);
        translate([0, 0, _height/2])
            indent(_inner_side, _radius, _thickness, _indent, _chamfer_h, _tolerance);
    }
}

module sopra(_inner_side, _radius, _thickness, _height, _indent, _chamfer_h) {
    union()
    {
        linear_extrude(_thickness)
            base(_inner_side, _radius, _thickness);
        side(_inner_side, _radius, _thickness, _height/2);
    }
}

sotto(inner_side, radius, thickness, height, indent, chamfer_h, tolerance);
translate([inner_side*2, 0, 0])
sopra(inner_side, radius, thickness, height);
